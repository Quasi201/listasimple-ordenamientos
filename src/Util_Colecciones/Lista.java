/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util_Colecciones;

import java.util.Objects;

/**
 *
 * @author quasi201
 * @param <T>
 */
public class Lista<T> {
    private Nodo<T> cab=null;
    private int tam=0;
    
    public Lista(){
        
    }
    private Nodo<T> getNodoPos(int pos) throws Exception{
        if(isEmpty() || pos<0 || pos>=tam){
            throw new Exception("La posicion solicitada no es valida");
        }
        else{
            int aux=0;
            Nodo<T> n;
            for(n = this.cab; n!=null ; n=n.getSiguiente()){
                if(aux==pos){
                    break;
                }
                aux++;
            }
            return n;
        }  
    }
    
    public void addBegin(T info){
        this.cab = new Nodo(info, this.cab);
        this.tam++;
    }
    
    public void addEnd(T info){
        if(isEmpty()){
            addBegin(info);
        }
        else{
            Nodo<T> ultimo;
            try{
                ultimo = getNodoPos(tam-1);
                ultimo.setSiguiente(new Nodo(info, null));
                this.tam++;
            } 
            catch (Exception ex){
                System.err.println("Error inesperado");
            }
        } 
    }
    public T get(int pos){
        Nodo<T> n;
        try {
            n = getNodoPos(pos);
            return n.getInfo();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }
    
    public void set(int pos, T nuevo){
        try {
            Nodo<T> n = getNodoPos(pos);
            n.setInfo(nuevo);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
      
    public void insertarOrdenado(T info){
        if(this.isEmpty())
            this.addBegin(info);
        else{
            Nodo<T> n = this.cab;
            Nodo<T> m = n;
            while(n!=null){
                Comparable c = (Comparable)info;
                int ans=c.compareTo(n.getInfo());
                if(ans<0)
                    break;
                m=n;
                n=n.getSiguiente();
            }
            if(n==m)
                this.addBegin(info);
            else{
                m.setSiguiente(new Nodo<>(info, n));
                this.tam++;
            }
        }
    }
    public void selectionSort(){
        if(isEmpty()) throw new RuntimeException("Lista Vacia");
        
        if(this.tam == 1)return;
        
        for(Nodo<T> actual = this.cab; actual!=null; actual=actual.getSiguiente()){
            Nodo<T> menor = getMenorNodo(actual);
            int c = this.comparador(menor.getInfo(), actual.getInfo());
            if(c<0){
                intercambiarInfo(menor, actual);
            }
        }
    }
    
    private int comparador(T info1, T info2){
        Comparable c = (Comparable)info1;
        return c.compareTo(info2);
    }
    
    private Nodo<T> getMenorNodo(Nodo<T> actualMenor){
        for(Nodo<T> n=actualMenor.getSiguiente(); n!=null; n=n.getSiguiente()){
            T info1 = actualMenor.getInfo();
            T info2 = n.getInfo();
            int c = comparador(info1, info2);
            if(c>0)
                actualMenor=n;
        }
        return actualMenor;
    }
            
    
    private void intercambiarInfo(Nodo<T> n, Nodo<T> m){
        T aux = n.getInfo();
        n.setInfo(m.getInfo());
        m.setInfo(aux);
    }
    
    public void insertionSortInfo(){
        for(Nodo<T> n=this.cab.getSiguiente(); n!=null; n=n.getSiguiente()){         
            for(Nodo<T> m=this.cab; m!=n; m=m.getSiguiente()){
                if(comparador(n.getInfo() , m.getInfo()) < 0){
                    intercambiarInfo(n,m);
                }
            }
        }
    }
    private void intercambiarNodo(Nodo<T> n, Nodo<T> m){   
        Nodo<T> prevN= null, auxN = m; //Buscar el nodo previo a n/
        while(auxN != null && auxN != n){
            prevN = auxN;
            auxN = auxN.getSiguiente();
        }
        
        Nodo<T> prevM= null, auxM = this.cab; //Buscar el nodo previo a m/
        while(auxM != null && auxM!= m){
            prevM = auxM;
            auxM = auxM.getSiguiente();
        }                      
        if(prevM != null){
            prevM.setSiguiente(auxN);
            prevN.setSiguiente(auxN.getSiguiente());
            auxN.setSiguiente(m);
        }
        else{          
            prevN.setSiguiente(auxN.getSiguiente());
            auxN.setSiguiente(m); 
            this.cab = auxN;   
        }
    }
    
    public void insertSortNodo(){
        for(Nodo<T> n=this.cab.getSiguiente(); n!=null; n=n.getSiguiente()){
            for(Nodo<T> m = this.cab; m.getSiguiente() != n; m=m.getSiguiente()){
                if(comparador(n.getInfo(), m.getInfo()) < 0){ 
                    intercambiarNodo(n, m);
                    break;
                }
            } 
        }
    }
    
    public boolean isEmpty(){
        return this.tam == 0;
    }
    
    @Override
    public String toString(){
        String msg="";
        for(Nodo<T> n=this.cab; n!=null; n=n.getSiguiente()){
            msg+=n.getInfo().toString()+" ";
        }
        return msg;      
    }
    
    public int getTamaño(){
        return this.tam;
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + Objects.hashCode(this.cab);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Lista<T> other = (Lista<T>) obj;
        Nodo<T> a = this.cab;
        Nodo<T> b = other.cab;
        while (a != null && b != null) {
            T info1 = a.getInfo();
            T info2 = b.getInfo();
            if (!info1.equals(info2)) {
                return false;
            }
            a = a.getSiguiente();
            b = b.getSiguiente();
        }
        return a == null && b == null;
    }
    
    public T remove(int pos){//Metodo realizado en clase por le profesor
        if(isEmpty())
            return null;
        Nodo<T> borrar;
        if(pos==0){
            borrar = this.cab;
            this.cab = this.cab.getSiguiente();
            borrar.setSiguiente(null);
            this.tam--;
        }
        else{
            try {
                Nodo<T> antes = getNodoPos(pos-1);
                borrar = antes.getSiguiente();
                antes.setSiguiente(borrar.getSiguiente());
                borrar.setSiguiente(null);
                
            } catch (Exception ex) {
                System.err.println(ex.getMessage());
                return null;
            }
            
        }
        this.tam--;
        return borrar.getInfo();
    }
    
    public void eliminarRepetidos(){
        if(this.isEmpty())
            return;
        
        for(Nodo<T> n = this.cab; n!=null; n=n.getSiguiente()){
            for(Nodo<T> m = n.getSiguiente(); m!=null;){
                if(n.getInfo().equals(m.getInfo())){
                    Nodo<T> tmp = m.getSiguiente();
                    eliminar(m,n);
                    m = tmp;
                }
                else m=m.getSiguiente();
            }
        }
    }
    
    private void eliminar(Nodo<T> m, Nodo<T> n){//Metodo para eliminar duplicados
        Nodo<T> aux = n;
        while(aux.getSiguiente()!= m){
            aux = aux.getSiguiente();
        }
        aux.setSiguiente(m.getSiguiente());
        m.setSiguiente(null);
        this.tam--;
    }
}