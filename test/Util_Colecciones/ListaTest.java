/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util_Colecciones;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author reneplast
 */
public class ListaTest {
    
    public ListaTest() {
    }

    @Test
    public void testAddEnd() {
        Lista <Integer> l1 = new Lista();
        l1.addEnd(1);
        l1.addEnd(2);
        l1.addEnd(3);
        l1.addEnd(4);
        l1.addEnd(5);
        boolean test1 = false;
        if(l1.get(0)==1 && l1.get(1)==2 && l1.get(2)==3 && l1.get(3)==4 && l1.get(4)==5)
            test1 = true;
        Lista <String> l2 = new Lista();
        l2.addEnd("Felipe");
        l2.addEnd("Maria");
        l2.addEnd("Pedro");
        l2.addEnd("Juan");
        l2.addEnd("Camilo");
        boolean test2 = false;
        if(l2.get(0)=="Felipe" && l2.get(1)=="Maria" && l2.get(2)=="Pedro" && l2.get(3)=="Juan" && l2.get(4)=="Camilo")
            test2 = true;
        
        assertTrue(test1 && test2);
    }

    @Test
    public void testSet() {
        Lista <Integer> l1 = new Lista();
        for(int i = 5 ; i>0; i--){
            l1.addEnd(i);
        }
        l1.set(0, 101);
        l1.set(1, 102);
        l1.set(2, 103);
        boolean test1 = false;
        if(l1.get(0)==101 && l1.get(1)==102 && l1.get(2)==103 && l1.get(3)==2 && l1.get(4)==1){
            test1 = true;
        } 
        Lista<String> l2 = new Lista();
        l2.addEnd("santiago");
        l2.addEnd("felipe");
        l2.addEnd("alferez");
        l2.addEnd("villamizar");
        l2.addEnd("diego");
        
        l2.set(0, "Jaime");
        l2.set(1, "Daniela");
        l2.set(2, "Luis");
        boolean test2=false;
        if(l2.get(0)=="Jaime" && l2.get(1)=="Daniela" && l2.get(2)=="Luis" && l2.get(3)=="villamizar" && l2.get(4)=="diego"){
            test2 = true;
        }
        assertTrue(test2 && test1);
    }

    @Test
    public void testInsertarOrdenado() {
        /**********Test1**************/
        Lista<Integer> l1 = new Lista();
        Lista<Integer> aux = new Lista();
        for(int i=0; i<5; i++){
            int x = (int)(Math.random()*100+1);
            l1.insertarOrdenado(x);
            aux.addEnd(x);          
        }
        aux.selectionSort();
        boolean test1 = false;
        if(l1.equals(aux))
            test1 = true;
        
        /************Test2**************/  
        
        Lista<Integer> l2 = new Lista();
        Lista<Integer> tmp = new Lista();
        for(int i=0; i<5; i++){
            int x = (int)(Math.random()*100+1);
            l2.insertarOrdenado(x);
            tmp.addEnd(x);          
        }
        tmp.selectionSort();
        boolean test2 = false;
        if(l2.equals(tmp))
            test2 = true;
        assertTrue(test1 && test2);
    }

    @Test
    public void testSelectionSort() {
        Lista<Integer> l1 = new Lista();
        for(int i=5; i>=1; i--){
            l1.addEnd(i);
        }
        l1.selectionSort();
        boolean test1 = false;
        if(l1.get(0)==1 && l1.get(1)==2 && l1.get(2)==3 && l1.get(3)==4 && l1.get(4)==5)
            test1 = true;
        assertTrue(test1);
        /************Test2**************/  
        Lista<String> l2 = new Lista();
        l2.addEnd("felipe");
        l2.addEnd("maria");
        l2.addEnd("pepe");
        l2.addEnd("juan");
        l2.addEnd("xiomara");
        l2.selectionSort();
        boolean test2=false;
        if("felipe".equals(l2.get(0)) && "juan".equals(l2.get(1)) && "maria".equals(l2.get(2)) && "pepe".equals(l2.get(3)) && "xiomara".equals(l2.get(4)))
            test2 = true;
        assertTrue(test2);
    }
}
